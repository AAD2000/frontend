import React, {useState} from "react";

import DropTableModal from "./modals/drop-table/DropTableModal";
import axios from "axios";
import ErrorModal from "./modals/ErrorModal";

const bound = {
  padding: "15px",
  border: "1px",
  backgroundColor: "#F2F2F2",
  marginRight: "10px",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const boundDeployed = {
  padding: "15px",
  border: "1px",
  marginRight: "10px",
  backgroundColor: "#C2C2DF",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const tableStyle = {
  minWidth: "240px",
  marginLeft: "-15px",
  marginRight: "-15px"
}

const liStyle = {
  listStyleType: "none",
  marginBottom: "20px",
  marginLeft: 0
}

const tableName = {
  fontSize: "20pt",
  fontFamily: "Roboto",
  fontWeight: "Bold"
}

const spacer = {
  width: "50px",
}
const buttons = {}

const separator = {
  height: "20px"

}

const bdelete = {
  padding: "5px 10px 5px 10px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "red",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  color: "red",
  zIndex: 500,
}
const td = {
  fontFamily: "Roboto",
  fontSize: "12pt",
}
const firsttd = {
  fontSize: "12pt",
  fontFamily: "Roboto",
  paddingLeft: "15px"
}

const bedit = {
  padding: "5px 20px 5px 20px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "#1B2CCA",
  backgroundColor: "#1B2CCA",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  marginRight: "10px",
  color: "white",
  zIndex: 500,
}

function DropTableFunction(props) {


  const [error, setError] = useState(false)
  const [errorMessage, setErrorMessage] = useState("")
  const [modal, setModal] = useState({
    modal1: {
      edit: false,
      mode: false,
      delete: false
    }
  })

  const deleteyId=()=>{
    axios.delete(`https://odd-login-app.herokuapp.com/api/liquibase-generator/drop-table/${props.data.id}?model=${props.model}`,{withCredentials: true}).then(
        (req)=>{
          if(req.data.status === "error"){
            setErrorMessage(req.data.message)
            setError(true)
          }else {
            props.update()
          }
        })
  }

  return (
      <li style={liStyle}>
        <div className="box" onClick={() => {
          setModal({modal1: {mode: true}})
        }} style={props.deployed ? boundDeployed : bound}>

          <h2>
            Drop Table: {props.data.name}
          </h2>
          <div style={separator}/>
          <div hidden={props.deployed} style={separator}/>
          <div hidden={props.deployed} style={buttons}>
            <button className="delete"
                    style={bdelete} onClick={(event) => {
                      deleteyId()

              event.stopPropagation();
            }}>DELETE
            </button>
          </div>

          </div>



        <DropTableModal isOpened={modal.modal1.mode} data={props.data}
                     onClose={() => {
                       setModal(
                           {
                             ...modal,
                             modal1: {
                               mode: false,
                               edit: false,
                               delete: false
                             }
                           });

                     }}/>
        <ErrorModal isOpened={error} message={errorMessage} onClose = {()=>{setError(false)}}/>

      </li>
  );
};

export default DropTableFunction;