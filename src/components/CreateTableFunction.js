import React, {useState} from "react";

import CreateTableModal from "./modals/create-table/CreateTableModal";
import EditCreateTableModal from "./modals/create-table/EditCreateTableModal";
import DeleteTableModal from "./modals/create-table/DeleteTableModal";

const bound = {
  padding: "15px",
  border: "1px",
  backgroundColor: "#F2F2F2",
  marginRight: "10px",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const boundDeployed = {
  padding: "15px",
  border: "1px",
  marginRight: "10px",
  backgroundColor: "#C2C2DF",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const tableStyle = {
  minWidth: "240px",
  marginLeft: "-15px",
  marginRight: "-15px"
}

const liStyle = {
  listStyleType: "none",
  marginBottom: "20px",
  marginLeft: 0
}

const tableName = {
  fontSize: "20pt",
  fontFamily: "Roboto",
  fontWeight: "Bold"
}

const spacer = {
  width: "50px",
}
const buttons = {}

const separator = {
  height: "20px"

}

const bdelete = {
  padding: "5px 10px 5px 10px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "red",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  color: "red",
  zIndex: 500,
}
const td = {
  fontFamily: "Roboto",
  fontSize: "12pt",
}
const firsttd = {
  fontSize: "12pt",
  fontFamily: "Roboto",
  paddingLeft: "15px"
}

const bedit = {
  padding: "5px 20px 5px 20px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "#1B2CCA",
  backgroundColor: "#1B2CCA",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  marginRight: "10px",
  color: "white",
  zIndex: 500,
}

function useForceUpdate() {
  let [value, setState] = useState(true);
  return () => setState(!value);
}

function CreateTableFunction(props) {

  let forceUpdate = useForceUpdate();

  const [modal, setModal] = useState({
    modal1: {
      edit: false,
      mode: false,
      delete: false
    }
  })
  return (
      <li style={liStyle}>
        <div className="box" onClick={() => {
          setModal({modal1: {mode: true, edit: false, delete:false}})
        }} style={props.deployed ? boundDeployed : bound}>
          <div style={tableName}>
            {props.data.name}
          </div>
          <div style={separator}/>
          <div>

            <table style={tableStyle}
                   cellspacing="0">{props.data.tableColumns.map(tc => {
              return <tr className="tr">
                <td style={firsttd}>{tc.name}</td>
                <td style={spacer}></td>
                <td style={td}>{tc.type}</td>
                <td style={spacer}></td>
                <td style={td}>
                  <label className="container">pk
                    <input type="checkbox"
                           checked={tc.primaryKey == null ? false
                               : tc.primaryKey}/>
                    <span className="checkmark"></span>
                  </label>
                </td>
                <td style={td}>
                  <label className="container">unique
                    <input type="checkbox"
                           checked={tc.unique == null ? false : tc.unique}/>
                    <span className="checkmark"></span>
                  </label>
                </td>
                <td style={td}>
                  <label className="container">nullable
                    <input type="checkbox"
                           checked={tc.nullable == null ? false : tc.nullable}/>
                    <span className="checkmark"></span>
                  </label>
                </td>
                <td style={td}>
                  <label className="container">increment
                    <input type="checkbox"
                           checked={tc.autoIncrement == null ? false
                               : tc.autoIncrement}/>
                    <span className="checkmark"></span>
                  </label>
                </td>
              </tr>
            })}</table>

          </div>
          <div style={separator}/>
          <div hidden={props.deployed} style={separator}/>
          <div hidden={props.deployed} style={buttons}>
            <button style={bedit} onClick={(event) => {
              setModal({...modal, modal1: {mode: false, edit: true, delete: false}})
              event.stopPropagation();
            }}>EDIT
            </button>
            <button className="delete"
                    style={bdelete} onClick={(event) => {
              setModal({...modal, modal1: {mode: false, edit: false, delete: true}})
              event.stopPropagation();
            }}>DELETE
            </button>
          </div>
        </div>
        <CreateTableModal isOpened={modal.modal1.mode} data={props.data}
                          onClose={() => {
                            setModal(
                                {...modal, modal1: {mode: false, edit: false, delete: false}})
                          }}/>
        <EditCreateTableModal model={props.model} v={props.v} isOpened={modal.modal1.edit}
                              data={props.data} onClose={() => {

          setModal({...modal, modal1: {mode: false, edit: false, delete: false}})
          // eslint-disable-next-line no-restricted-globals
          props.update()
        }
        }/>
        <DeleteTableModal model={props.model} id={props.data.id} isOpened={modal.modal1.delete} name={props.data.name} onClose={() => {

          setModal({...modal, modal1: {mode: false, edit: false, delete: false}})
          // eslint-disable-next-line no-restricted-globals
          props.update()
          }}/>
      </li>
  );
}

export default CreateTableFunction;