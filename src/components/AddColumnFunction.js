import React, {useState} from "react";

import CreateTableModal from "./modals/create-table/CreateTableModal";
import EditCreateTableModal from "./modals/create-table/EditCreateTableModal";
import DeleteTableModal from "./modals/create-table/DeleteTableModal";
import CreateConnectionModal
  from "./modals/create-connection/CreateConnectionModal";
import AddColumnModal from "./modals/add-column/AddColumnModal";
import DeleteAddColumnModal from "./modals/add-column/DeleteAddColumnModal";
import EditAddColumnModal from "./modals/add-column/EditAddColumnModal";

const bound = {
  padding: "15px",
  border: "1px",
  backgroundColor: "#F2F2F2",
  marginRight: "10px",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const boundDeployed = {
  padding: "15px",
  border: "1px",
  marginRight: "10px",
  backgroundColor: "#C2C2DF",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const tableStyle = {
  minWidth: "240px",
  marginLeft: "-15px",
  marginRight: "-15px"
}

const liStyle = {
  listStyleType: "none",
  marginBottom: "20px",
  marginLeft: 0
}

const tableName = {
  fontSize: "20pt",
  fontFamily: "Roboto",
  fontWeight: "Bold"
}

const spacer = {
  width: "50px",
}
const buttons = {}

const separator = {
  height: "20px"

}

const bdelete = {
  padding: "5px 10px 5px 10px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "red",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  color: "red",
  zIndex: 500,
}
const td = {
  fontFamily: "Roboto",
  fontSize: "12pt",
}
const firsttd = {
  fontSize: "12pt",
  fontFamily: "Roboto",
  paddingLeft: "15px"
}

const bedit = {
  padding: "5px 20px 5px 20px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "#1B2CCA",
  backgroundColor: "#1B2CCA",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  marginRight: "10px",
  color: "white",
  zIndex: 500,
}

function AddColumnFunction(props) {


  const [modal, setModal] = useState({
    modal1: {
      edit: false,
      mode: false,
      delete: false
    }
  })
  return (
      <li style={liStyle}>
        <div className="box" onClick={() => {
          setModal({modal1: {mode: true, edit: false, delete:false}})
        }} style={props.deployed ? boundDeployed : bound}>
          <div style={tableName}>
            Add to {props.data.tableName}
          </div>
          <div style={separator}/>
          <div>

            <table style={tableStyle}
                   cellspacing="0">
              <tr className="tr">
                <td style={firsttd}>{props.data.name}</td>
                <td style={spacer}></td>
                <td style={td}>{props.data.type}</td>
                <td style={spacer}></td>
                <td style={td}>
                  <label className="container">pk
                    <input type="checkbox"
                           checked={props.data.primaryKey == null ? false
                               : props.data.primaryKey}/>
                    <span className="checkmark"></span>
                  </label>
                </td>
                <td style={td}>
                  <label className="container">unique
                    <input type="checkbox"
                           checked={props.data.unique == null ? false : props.data.unique}/>
                    <span className="checkmark"></span>
                  </label>
                </td>
                <td style={td}>
                  <label className="container">nullable
                    <input type="checkbox"
                           checked={props.data.nullable == null ? false : props.data.nullable}/>
                    <span className="checkmark"></span>
                  </label>
                </td>
                <td style={td}>
                  <label className="container">increment
                    <input type="checkbox"
                           checked={props.data.autoIncrement == null ? false
                               : props.data.autoIncrement}/>
                    <span className="checkmark"></span>
                  </label>
                </td>
              </tr>
            </table>

          </div>
          <div style={separator}/>
          <div hidden={props.deployed} style={separator}/>
          <div hidden={props.deployed} style={buttons}>
            <button style={bedit} onClick={(event) => {
              setModal({...modal, modal1: {mode: false, edit: true, delete: false}})
              event.stopPropagation();
            }}>EDIT
            </button>
            <button className="delete"
                    style={bdelete} onClick={(event) => {
              setModal({...modal, modal1: {mode: false, edit: false, delete: true}})
              event.stopPropagation();
            }}>DELETE
            </button>
          </div>
        </div>

          <AddColumnModal model={props.model} isOpened={modal.modal1.mode} data={props.data}
                                 onClose={() => {
                                   setModal(
                                       {
                                         ...modal,
                                         modal1: {
                                           mode: false,
                                           edit: false,
                                           delete: false
                                         }
                                       });

                                 }}/>
        <DeleteAddColumnModal model={props.model} isOpened={modal.modal1.delete} id={props.data.id} name={props.data.name} tableName={props.data.tableName}  onClose={() => {
          setModal(
              {
                ...modal,
                modal1: {
                  mode: false,
                  edit: false,
                  delete: false
                }
              });
          props.update()
        }}/>
        <EditAddColumnModal model={props.model} isOpened={modal.modal1.edit} data={props.data} v={props.v} onClose={() => {
          setModal(
              {
                ...modal,
                modal1: {
                  mode: false,
                  edit: false,
                  delete: false
                }
              });
          props.update()
        }}/>
      </li>
  );
}

export default AddColumnFunction;