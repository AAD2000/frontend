import React, {useState} from "react";
import axios from "axios";

const bound = {
  padding: "15px",
  border: "1px",
  marginLeft: "154px",
  marginRight:"155px",
  backgroundColor: "#F2F2F2",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const boundDeployed = {
  padding: "15px",
  border: "1px",
  backgroundColor: "#C2C2DF",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const tableStyle = {
  minWidth: "678px",
  marginLeft: "-15px",
  marginRight: "-15px"
}

const liStyle = {
  listStyleType: "none",
  marginBottom: "20px",
  marginLeft: 0
}

const tableName = {
  fontSize: "20pt",
  fontFamily: "Roboto",
  fontWeight: "Bold"
}

const spacer = {
  width: "50px",
}
const buttons = {}

const separator = {
  height: "20px"

}

const bdelete = {
  padding: "5px 10px 5px 10px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "red",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  color: "red"
}
const td = {
  fontFamily: "Roboto",
  fontSize: "12pt",
}
const firsttd = {
  fontSize: "12pt",
  fontFamily: "Roboto",
  paddingLeft: "15px"
}

const bedit = {
  padding: "5px 20px 5px 20px",
  border: "1px",
  borderRadius: "4px",
  marginLeft: "20px",
  borderColor: "#1B2CCA",
  backgroundColor: "#1B2CCA",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  color: "white"
}
const close = {
  float: "right",
  color: "grey",
  border: "1px",
  padding: "2px",
  borderRadius: "2px",
  fontSize: "10pt",
  borderStyle: "solid"
}
const lableStyle = {
  marginLeft: "10px"
}

const CreateVersionModel = (props) => {

  const [major, setMajor] = useState(false)

  const onButtonClicked=()=>{
    props.onClose()
  }

  const createVersion = (major) => {

    axios.post(
        `https://odd-login-app.herokuapp.com/api/liquibase-generator/version?dc=${props.id}&major=${major}`,null,{withCredentials: true}).then(
        response => {
          props.update()
          props.onClose()
        },)
  }



  return (
      <div className={`modal_wrapper ${props.isOpened ? 'open' : 'close'}`}>
        <div className="modal_body">

          <div className="box" style={bound}>
            <div style={close} onClick={props.onClose}>exit</div>
            <div style={separator}/>

            <input type="checkbox" checked={major} onChange={(e)=>{setMajor(e.target.checked)}}/><label style={lableStyle}>major</label><button style={bedit} onClick={()=>createVersion(major)}>ADD</button>
          </div>
        </div>
      </div>
  );

}

export default CreateVersionModel;