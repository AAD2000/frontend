import React, {useState} from "react";
import axios from "axios";
import ErrorModal from "../ErrorModal";

const bound = {
  padding: "15px",
  border: "1px",
  marginLeft: "154px",
  marginRight:"155px",
  backgroundColor: "#F2F2F2",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const boundDeployed = {
  padding: "15px",
  border: "1px",
  backgroundColor: "#C2C2DF",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const tableStyle = {
  minWidth: "678px",
  marginLeft: "-15px",
  marginRight: "-15px"
}

const liStyle = {
  listStyleType: "none",
  marginBottom: "20px",
  marginLeft: 0
}

const tableName = {
  fontSize: "20pt",
  fontFamily: "Roboto",
  fontWeight: "Bold"
}

const spacer = {
  width: "50px",
}
const buttons = {}

const separator = {
  height: "20px"

}

const bdelete = {
  padding: "5px 10px 5px 10px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "red",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  color: "red"
}
const td = {
  fontFamily: "Roboto",
  fontSize: "12pt",
}
const firsttd = {
  fontSize: "12pt",
  fontFamily: "Roboto",
  paddingLeft: "15px"
}

const bedit = {
  padding: "5px 20px 5px 20px",
  border: "1px",
  borderRadius: "4px",
  marginLeft: "0px",
  borderColor: "#1B2CCA",
  backgroundColor: "#1B2CCA",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  color: "white"
}
const close = {
  float: "right",
  color: "grey",
  border: "1px",
  padding: "2px",
  borderRadius: "2px",
  fontSize: "10pt",
  borderStyle: "solid"
}

const AddDropTableModal = (props) => {

  const [error, setError] = useState(false)
  const [errorMessage, setErrorMessage] = useState("")
  const [check, setCheck] = useState(false)
  const [value, setValue] = useState("")

  const addTable=()=>{
    axios.post(`https://odd-login-app.herokuapp.com/api/liquibase-generator/drop-table?v=${props.v}&model=${props.model}`,{
      name:value,
      cascade: check
    },{withCredentials: true}).then((req)=>{
      if(req.data.status === "error"){
        setErrorMessage(req.data.message)
        setError(true)
      }else {
        props.onClose()
      }
    })
  }

  return (
      <div className={`modal_wrapper ${props.isOpened ? 'open' : 'close'}`}>
        <div className="modal_body">
          <div className="box" style={bound}>
            <div style={close} onClick={props.onClose}>exit</div>
            <div style={separator}></div>
            <div style={separator}></div>
            <div><input type="checkbox" checked={check} onChange={(e)=>setCheck(e.target.checked)}/><label>Cascade</label></div>
            <div><label>Drop table name: </label><input value={value} onChange={(e)=>setValue(e.target.value)}/></div>
            <div style={separator}/>
            <button style={bedit} onClick={()=>{ addTable();}}>ADD</button>
          </div>
        </div>
        <ErrorModal isOpened={error} message={errorMessage} onClose = {()=>{setError(false)}}/>
      </div>
  );

}

export default AddDropTableModal;