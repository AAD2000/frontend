import React, {useState} from "react";
import axios from "axios";

const bound = {
    padding: "15px",
    border: "1px",
    backgroundColor: "#F2F2F2",
    borderColor: "#C2C2C2",
    borderStyle: "solid",
    borderRadius: "4px",
    minWidth: "200px"
}

const boundDeployed = {
    padding: "15px",
    border: "1px",
    backgroundColor: "#C2C2DF",
    borderColor: "#C2C2C2",
    borderStyle: "solid",
    borderRadius: "4px",
    minWidth: "200px"
}

const tableStyle = {
    minWidth: "678px",
    marginLeft: "-15px",
    marginRight: "-15px"
}

const liStyle = {
    listStyleType: "none",
    marginBottom: "20px",
    marginLeft: 0
}

const tableName = {
    fontSize: "20pt",
    fontFamily: "Roboto",
    fontWeight: "Bold"
}

const spacer = {
    width: "50px",
}
const buttons = {}

const separator = {
    height: "20px"

}

const bdelete = {
    padding: "5px 10px 5px 10px",
    border: "1px",
    borderRadius: "4px",
    borderColor: "red",
    borderStyle: "solid",
    fontFamily: "Roboto",
    fontSize: "10pt",
    color: "red"
}
const td = {
    fontFamily: "Roboto",
    fontSize: "12pt",
}
const firsttd = {
    fontSize: "12pt",
    fontFamily: "Roboto",
    paddingLeft: "15px"
}

const bedit = {
    padding: "5px 20px 5px 20px",
    border: "1px",
    borderRadius: "4px",
    borderColor: "#1B2CCA",
    backgroundColor: "#1B2CCA",
    borderStyle: "solid",
    fontFamily: "Roboto",
    fontSize: "10pt",
    marginRight: "10px",
    color: "white"
}
const close = {
    float: "right",
    color: "grey",
    border: "1px",
    padding: "2px",
    borderRadius: "2px",
    fontSize: "10pt",
    borderStyle: "solid"
}
const lableStyle = {
    marginLeft: "10px"
}

const CreateDockerComposeModel = (props) => {

    const [created, setCreated] = useState(false)
    const [modelName, setModelName] = useState("")
    const [host, setHostName] = useState(null)
    const [port, setPortName] = useState("")
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const [id, setId] = useState(null)
    const [database, setDatabase] = useState("")

    const onButtonClicked = () => {
        postData()
        props.setIdInModel(id);
        props.onClose()
    }

    const postData = () => {
        axios.post("https://odd-login-app.herokuapp.com/api/liquibase-generator/docker-compose", created ? {
            modelName: modelName,
            openPort: port,
            created: created,
            password: password,
            userName: username,
            host: host,
            database: document.getElementById("selectBox").options[document.getElementById("selectBox").selectedIndex].value
        } : {modelName: modelName, database: document.getElementById("selectBox").options[document.getElementById("selectBox").selectedIndex].value}, {withCredentials: true}).then(response => {
            props.setIdInModel(response.data.id)
        })
    }


    return (
        <div className={`modal_wrapper ${props.isOpened ? 'open' : 'close'}`}>
            <div className="modal_body">

                <div style={bound}>
                    <div style={close} onClick={props.onClose}>exit</div>
                    <div>
                        <input type="checkbox" checked={created} onChange={(e) => {
                            setCreated(e.target.checked)
                        }}/><label style={lableStyle}>created</label>
                    </div>
                    <div style={separator}/>
                    <div>
                        <input value={modelName} onChange={(e) => {
                            setModelName(e.target.value)
                        }}/><label style={lableStyle}>model name</label>
                    </div>
                    <div style={separator}/>
                    {/*<script type="text/javascript">*/}
                    {/**/}
                    {/*  function changeFunc() {*/}
                    {/*  var selectBox = document.getElementById("selectBox");*/}
                    {/*  var selectedValue = document.getElementById("selectBox").selectBox.options[selectBox.selectedIndex].value;*/}
                    {/*  console.log(selectedValue);*/}
                    {/*}*/}

                    <div>
                        <select id="selectBox">
                            <option value="postgres">PostgreSQL</option>
                            <option value="mysql">MySql</option>
                        </select>
                    </div>
                    <div style={separator}/>

                    <div hidden={!created}>
                        <div>
                            <input value={host} onChange={(e) => {
                                setHostName(e.target.value)
                            }}/><label style={lableStyle}>host</label>
                        </div>
                        <div style={separator}/>
                        <div>
                            <input value={port} onChange={(e) => {
                                setPortName(e.target.value)
                            }}/><label style={lableStyle}>port</label>
                        </div>
                        <div style={separator}/>
                        <div>
                            <input value={username} onChange={(e) => {
                                setUsername(e.target.value)
                            }}/><label style={lableStyle}>username</label>
                        </div>
                        <div style={separator}/>
                        <div>
                            <input value={password} onChange={(e) => {
                                setPassword(e.target.value)
                            }}/><label style={lableStyle}>password</label>
                        </div>
                        <div style={separator}/>
                    </div>
                    <button style={bedit} onClick={onButtonClicked}>ADD</button>
                </div>
            </div>
        </div>
    );

}

export default CreateDockerComposeModel;