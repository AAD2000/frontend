import React from "react";

const bound = {
  padding: "15px",
  border: "1px",
  backgroundColor: "#F2F2F2",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const boundDeployed = {
  padding: "15px",
  border: "1px",
  backgroundColor: "#C2C2DF",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const tableStyle = {
  minWidth: "678px",
  marginLeft: "-15px",
  marginRight: "-15px"
}

const liStyle = {
  listStyleType: "none",
  marginBottom: "20px",
  marginLeft: 0
}

const tableName = {
  fontSize: "20pt",
  fontFamily: "Roboto",
  fontWeight: "Bold"
}

const spacer = {
  width: "50px",
}
const buttons = {}

const separator = {
  height: "20px"

}

const bdelete = {
  padding: "5px 10px 5px 10px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "red",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  color: "red"
}
const td = {
  fontFamily: "Roboto",
  fontSize: "12pt",
}
const firsttd = {
  fontSize: "12pt",
  fontFamily: "Roboto",
  paddingLeft: "15px"
}

const bedit = {
  padding: "5px 20px 5px 20px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "#1B2CCA",
  backgroundColor: "#1B2CCA",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  marginRight: "10px",
  color: "white"
}
const close = {
  float: "right",
  color: "grey",
  border: "1px",
  padding: "2px",
  borderRadius: "2px",
  fontSize: "10pt",
  borderStyle: "solid"
}

const CreateTableModal = (props) => {

  return (
      <div className={`modal_wrapper ${props.isOpened ? 'open' : 'close'}`}>
        <div className="modal_body">
          <div className="box" style={props.deployed ? boundDeployed : bound}>
            <div style={close} onClick={props.onClose}>exit</div>
            <div style={tableName}>
              {props.data.name}
            </div>
            <div style={separator}/>
            <div>
              <table style={tableStyle}
                     cellSpacing="0">{props.data.tableColumns.map(tc => {
                return <tr className="tr">
                  <td style={firsttd}>{tc.name}</td>
                  <td style={spacer}></td>
                  <td style={td}>{tc.type}</td>
                  <td style={spacer}></td>
                  <td style={td}>
                    <label className="container">pk
                      <input type="checkbox"
                             checked={tc.primaryKey == null ? false
                                 : tc.primaryKey}/>
                      <span className="checkmark"></span>
                    </label>
                  </td>
                  <td style={td}>
                    <label className="container">unique
                      <input type="checkbox"
                             checked={tc.unique == null ? false : tc.unique}/>
                      <span className="checkmark"></span>
                    </label>
                  </td>
                  <td style={td}>
                    <label className="container">nullable
                      <input type="checkbox"
                             checked={tc.nullable == null ? false
                                 : tc.nullable}/>
                      <span className="checkmark"></span>
                    </label>
                  </td>
                  <td style={td}>
                    <label className="container">increment
                      <input type="checkbox"
                             checked={tc.autoIncrement == null ? false
                                 : tc.autoIncrement}/>
                      <span className="checkmark"></span>
                    </label>
                  </td>
                </tr>
              })}</table>
            </div>
            <div style={separator}/>
          </div>
        </div>
      </div>
  );

}

export default CreateTableModal;