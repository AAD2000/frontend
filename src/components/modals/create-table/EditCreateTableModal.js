import React, {useState} from "react";
import axios from "axios";
import EditTableColumn from "./EditTableColumn";
import ErrorModal from "../ErrorModal";

const tableStyle = {
    minWidth: "550px",
    marginLeft: "-15px",
    marginRight: "-15px"
}

const close = {
    float: "right",
    color: "grey",
    border: "1px",
    padding: "2px",
    borderRadius: "2px",
    fontSize: "10pt",
    borderStyle: "solid"
}

const bound = {
    padding: "15px",
    border: "1px",
    backgroundColor: "#F2F2F2",
    borderColor: "#C2C2C2",
    borderStyle: "solid",
    borderRadius: "4px",
    minWidth: "200px"
}

const nameLabel = {
    width: "104px",
    paddingRight: "16px",
    textAlign: "center"
}
const checkboxLabel = {
    width: "75px",
    textAlign: "center"
}

const bedit = {
    marginBottom: "20px",
    padding: "3px 20px 3px 20px",
    border: "1px",
    borderRadius: "4px",
    borderColor: "#1B2CCA",
    backgroundColor: "#1B2CCA",
    borderStyle: "solid",
    fontFamily: "Roboto",
    fontSize: "10pt",
    marginRight: "10px",
    color: "white",
    zIndex: 500,
}

const badd = {
    marginTop: "20px",
    padding: "3px 20px 3px 20px",
    border: "1px",
    borderRadius: "4px",
    borderColor: "#1B2CCA",
    backgroundColor: "#1B2CCA",
    borderStyle: "solid",
    fontFamily: "Roboto",
    fontSize: "10pt",
    marginRight: "10px",
    color: "white",
    zIndex: 500,
}

const tableName = {
    marginRight: "5px"
}

const EditCreateTableModal = (props) => {

    const [error, setError] = useState(false)
    const [errorMessage, setErrorMessage] = useState("")
    const [name, setName] = useState(props.data.name);
    const [inputList, setInputList] = useState(props.data.tableColumns.map(tc => {
        return <div><EditTableColumn model={props.model} data={tc} v={props.v} ct={props.data.id}/>
        </div>
    }));

    const update = () => {
        axios.put(
            `https://odd-login-app.herokuapp.com/api/liquibase-generator/create-table/${props.data.id}?v=${props.v}&model=${props.model}`,
            {
                id: props.data.id,
                name: name
            }, {withCredentials: true}).then((req) => {
            if (req.data.status === "error") {
                setErrorMessage(req.data.message)
                setError(true)
            } else {
            }
        })
    }

    const addColumn = () => {
        axios.post(
            `https://odd-login-app.herokuapp.com/api/liquibase-generator/table-column?v=${props.v}&ct=${props.data.id}`,
            {
                name: "",
                type: "",
            }, {withCredentials: true}).then((response) => {
            setInputList(
                inputList.concat(<div><EditTableColumn  model={props.model} ct={props.data.id} v={props.v}
                                                       data={{
                                                           id: response.data,
                                                           name: "",
                                                           type: ""
                                                       }}/></div>))
        })
    }


    return (
        <div className={`modal_wrapper ${props.isOpened ? 'open' : 'close'}`}>
            <div className="modal_body">
                <div style={bound}>
                    <div style={close} onClick={props.onClose}>exit</div>
                    <label>Table name: </label>
                    <input style={tableName} value={name} onChange={(e) => {
                        if (e.target.value.match("^[A-Za-z0-9_]*$")) {
                            setName(e.target.value)
                        }
                    }}/>
                    <button style={bedit} onClick={update}>SAVE</button>
                    <table>
                        <tr>
                            <td style={nameLabel}>Name</td>
                            <td style={nameLabel}>type</td>
                            <td style={checkboxLabel}>pk</td>
                            <td style={checkboxLabel}>unique</td>
                            <td style={checkboxLabel}>nullable</td>
                            <td>increment</td>
                        </tr>
                    </table>
                    {inputList}
                    <div>
                        <button style={badd} onClick={addColumn}>ADD COLUMN</button>
                    </div>
                </div>
            </div>
            <ErrorModal isOpened={error} message={errorMessage} onClose={() => {
                setError(false)
            }}/>

        </div>
    );
};

export default EditCreateTableModal