import React, {useState} from "react";
import axios from "axios";
import ErrorModal from "../ErrorModal";

const liStyle = {
  listStyleType: "none",
  marginBottom: "20px",
  marginLeft: 0
}

const tableName = {
  fontSize: "20pt",
  fontFamily: "Roboto",
  fontWeight: "Bold"
}

const spacer = {
  width: "50px",
}
const buttons = {}

const separator = {
  height: "5px"

}

const bdelete = {
  padding: "5px 10px 5px 10px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "red",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  color: "red",
  zIndex: 500,
}

const td = {
  fontSize: "12pt",
  fontFamily: "Roboto",
  width: "100px",
  marginRight: "15px",
}

const bedit = {
  padding: "5px 20px 5px 20px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "#1B2CCA",
  backgroundColor: "#1B2CCA",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  marginRight: "10px",
  color: "white",
  zIndex: 500,
}

const checkbox = {
  marginLeft: "34px",
  marginRight: "33.5px"
}

const buttonStyle = {
  backgroundColor: "transparent",
  color: "red",
  marginLeft: "15px",
  border: "none"
}

const EditTableColumn = (props) => {

    const [error, setError] = useState(false)
    const [errorMessage, setErrorMessage] = useState("")

    const [value, setValue] = useState(false)
  const [name, setName] = useState(props.data.name)
  const [type, setType] = useState(props.data.type)
  const [pk, setPk] = useState(
      props.data.primaryKey == null ? false : props.data.primaryKey)
  const [nullable, setNullable] = useState(
      props.data.nullable == null ? false : props.data.nullable)
  const [unique, setUnique] = useState(
      props.data.unique == null ? false : props.data.unique)
  const [increment, setIncrement] = useState(
      props.data.autoIncrement == null ? false : props.data.autoIncrement)
  const [save, setSave] = useState(true)

  const saveData = ()=>{
    axios.put(`https://odd-login-app.herokuapp.com/api/liquibase-generator/table-column/${props.data.id}?v=${props.v}&ct=${props.ct}&model=${props.model}`,{
      id: props.data.id,
      autoIncrement: increment,
      name: name,
      nullable: nullable,
      primaryKey: pk,
      type: type,
      unique: unique
    },{withCredentials: true}).then((req)=>{
        if(req.data.status === "error"){
            setErrorMessage(req.data.message)
            setError(true)
        }
    })
  }

  const deleteColumn = ()=>{
    axios.delete(`https://odd-login-app.herokuapp.com/api/liquibase-generator/table-column/${props.data.id}`,{withCredentials: true})
  }

  return (
      <div hidden={value}>

        <div style={separator}/>
        <input style={td} value={name}
               onChange={(e) => {
                 if (e.target.value.match("^[A-Za-z0-9_]*$")) {
                   setName(e.target.value)
                   setSave(false)
                 }
               }
               }></input>
        <input style={td} value={type} onChange={(e) => {
          if (e.target.value.match("^[A-Za-z0-9_()]*$")) {
            setType(e.target.value)
            setSave(false)
          }
        }
        }></input>
        <input style={checkbox} checked={pk} onChange={(e) => {
          setPk(e.target.checked)
          setSave(false)
        }} type="checkbox"/>
        <input style={checkbox} checked={unique} onChange={(e) => {
          setUnique(e.target.checked)
          setSave(false)
        }} type="checkbox"/>
        <input style={checkbox} checked={nullable} onChange={(e) => {
          setNullable(e.target.checked)
          setSave(false)
        }} type="checkbox"/>
        <input style={checkbox} checked={increment} onChange={(e) => {
          setIncrement(e.target.checked)
          setSave(false)
        }} type="checkbox"/>

        <button style={buttonStyle} onClick={() => {setValue(true); deleteColumn()}}>x</button>
        <button style={buttonStyle}  hidden={save} onClick={(e)=> {setSave(true); saveData()}}>💾</button>

        <div style={separator}/>
          <ErrorModal isOpened={error} message={errorMessage} onClose = {()=>{setError(false)}}/>

      </div>

  );

}

export default EditTableColumn;