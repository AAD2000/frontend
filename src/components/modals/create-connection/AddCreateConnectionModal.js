import axios from "axios";
import React, {useState} from "react";
import ErrorModal from "../ErrorModal";

const bound = {
  padding: "15px",
  border: "1px",
  backgroundColor: "#F2F2F2",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const boundDeployed = {
  padding: "15px",
  border: "1px",
  backgroundColor: "#C2C2DF",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const tableStyle = {
  minWidth: "678px",
  marginLeft: "-15px",
  marginRight: "-15px"
}

const liStyle = {
  listStyleType: "none",
  marginBottom: "20px",
  marginLeft: 0
}

const tableName = {
  fontSize: "20pt",
  fontFamily: "Roboto",
  fontWeight: "Bold"
}

const spacer = {
  width: "50px",
}
const buttons = {}

const separator = {
  height: "20px"

}

const bdelete = {
  padding: "5px 10px 5px 10px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "red",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  color: "red"
}
const td = {
  fontFamily: "Roboto",
  fontSize: "12pt",
  paddingLeft: "10px",
  paddingRight: "10px",
}
const firsttd = {
  fontSize: "12pt",
  fontFamily: "Roboto",
  paddingLeft: "15px"
}

const inputStyle={
  width: "100px"
}

const bedit = {
  padding: "5px 20px 5px 20px",
  border: "1px",
  borderRadius: "4px",
  marginTop: "30px",
  marginLeft: "0px",
  borderColor: "#1B2CCA",
  backgroundColor: "#1B2CCA",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  color: "white"
}
const close = {
  float: "right",
  color: "grey",
  border: "1px",
  padding: "2px",
  borderRadius: "2px",
  fontSize: "10pt",
  borderStyle: "solid"
}

const AddCreateConnectionModal=(props)=>{

  const [error, setError] = useState(false)
  const [errorMessage, setErrorMessage] = useState("")
  const [constraintName, setConstraintName] = useState("fk___")
  const [baseTableName, setBaseTableName] = useState("")
  const [baseColumnName, setBaseColumnName] = useState("")
  const [referencedColumnName, setReferencedColumnName] = useState("")
  const [referencedTableName, setReferencedTableName] = useState("")



  const addTable=()=>{
    axios.post(`https://odd-login-app.herokuapp.com/api/liquibase-generator/connection?v=${props.v}&model=${props.model}`,{
      constraintName: constraintName,
      baseTableName: baseTableName,
      baseColumnNames: baseColumnName,
      referencedColumnNames: referencedColumnName,
      referencedTableName: referencedTableName,
    },{withCredentials: true}).then((req)=>{if(req.data.status === "error"){
      setErrorMessage(req.data.message)
      setError(true)
    }else {
      props.onClose()
    }})
  }

  const generateConstraintName=(pbaseTableName, pbaseColumnName, preferencedTableName, preferencedColumnName)=>{
    setConstraintName(`fk_${pbaseTableName}_${pbaseColumnName}_${preferencedTableName}_${preferencedColumnName}`)
  }

  return (
      <div className={`modal_wrapper ${props.isOpened ? 'open' : 'close'}`}>
        <div className="modal_body">
          <div style={bound}>
            <div style={close} onClick={props.onClose}>exit</div>
            <div style={tableName}>
              {constraintName}
            </div>
            <div style={separator}/>
            <table style={tableStyle}
                   cellSpacing="0">
              <tr className="tr">
                <td style={td}>
                  Base table
                </td>
                <td style={td}>
                  Base column
                </td>
                <td style={td}>
                </td>
                <td style={td}>
                  Referenced table
                </td>
                <td style={td}>
                  Referenced column
                </td>
              </tr>
              <tr className="tr">
                <td style={td}>
                  <input style={inputStyle} value={baseTableName} onChange={(e)=> {setBaseTableName(e.target.value); generateConstraintName(e.target.value, baseColumnName, referencedTableName, referencedColumnName)}}/>
                </td>
                <td style={td}>
                  <input style={inputStyle} value={baseColumnName} onChange={(e)=> {setBaseColumnName(e.target.value); generateConstraintName(baseTableName, e.target.value, referencedTableName, referencedColumnName)}}/>
                </td>
                <td style={td}>
                  →
                </td>
                <td style={td}>
                  <input style={inputStyle} value={referencedTableName} onChange={(e)=> {setReferencedTableName(e.target.value); generateConstraintName(baseTableName, baseColumnName, e.target.value, referencedColumnName)}}/>
                </td>
                <td style={td}>
                  <input style={inputStyle} value={referencedColumnName} onChange={(e)=> {setReferencedColumnName(e.target.value); generateConstraintName(baseTableName, baseColumnName, referencedTableName, e.target.value)}}/>
                </td>
              </tr>
            </table>
            <button style={bedit} onClick={()=>addTable()}>
              ADD
            </button>
          </div>
        </div>
        <ErrorModal isOpened={error} message={errorMessage} onClose = {()=>{setError(false)}}/>
      </div>
  );
}

export default AddCreateConnectionModal