import axios from "axios";
import React, {useState} from "react";

const bound = {
  padding: "15px",
  border: "1px",
  backgroundColor: "#F2F2F2",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const boundDeployed = {
  padding: "15px",
  border: "1px",
  backgroundColor: "#C2C2DF",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const tableStyle = {
  minWidth: "678px",
  marginLeft: "-15px",
  marginRight: "-15px"
}

const liStyle = {
  listStyleType: "none",
  marginBottom: "20px",
  marginLeft: 0
}

const tableName = {
  fontSize: "20pt",
  fontFamily: "Roboto",
  fontWeight: "Bold"
}

const spacer = {
  width: "50px",
}
const buttons = {}

const separator = {
  height: "20px"

}

const bdelete = {
  padding: "5px 10px 5px 10px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "red",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  color: "red"
}
const td = {
  fontFamily: "Roboto",
  fontSize: "12pt",
  paddingLeft: "10px",
  paddingRight: "10px",
}
const firsttd = {
  fontSize: "12pt",
  fontFamily: "Roboto",
  paddingLeft: "15px"
}

const inputStyle = {
  width: "100px"
}

const bedit = {
  padding: "5px 20px 5px 20px",
  border: "1px",
  borderRadius: "4px",
  marginTop: "30px",
  marginLeft: "0px",
  borderColor: "#1B2CCA",
  backgroundColor: "#1B2CCA",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  color: "white"
}
const close = {
  float: "right",
  color: "grey",
  border: "1px",
  padding: "2px",
  borderRadius: "2px",
  fontSize: "10pt",
  borderStyle: "solid"
}

const CreateConnectionModal = (props) => {

  const [constraintName, setConstraintName] = useState(
      props.data.constraintName)
  const [baseTableName, setBaseTableName] = useState(props.data.baseTableName)
  const [baseColumnName, setBaseColumnName] = useState(
      props.data.baseColumnNames)
  const [referencedColumnName, setReferencedColumnName] = useState(
      props.data.referencedColumnNames)
  const [referencedTableName, setReferencedTableName] = useState(
      props.data.referencedTableName)

  return (
      <div className={`modal_wrapper ${props.isOpened ? 'open' : 'close'}`}>
        <div className="modal_body">
          <div style={bound}>
            <div style={close} onClick={props.onClose}>exit</div>
            <div style={tableName}>
              {constraintName}
            </div>
            <div style={separator}/>
            <table style={tableStyle}
                   cellSpacing="0">
              <tr className="tr">
                <td style={td}>
                  Base table
                </td>
                <td style={td}>
                  Base column
                </td>
                <td style={td}>
                </td>
                <td style={td}>
                  Referenced table
                </td>
                <td style={td}>
                  Referenced column
                </td>
              </tr>
              <tr className="tr">
                <td style={td}>
                  <label>{baseTableName}</label>
                </td>
                <td style={td}>
                  <label>{baseColumnName}</label>
                </td>
                <td style={td}>
                  →
                </td>
                <td style={td}>
                  <label>{referencedTableName}</label>
                </td>
                <td style={td}>
                  <label>{referencedColumnName}</label>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
  );
}

export default CreateConnectionModal