import React, {useState} from "react";
import EditTableColumn from "../create-table/EditTableColumn";
import EditIndexColumn from "./EditIndexColumn";
import axios from "axios";
import ErrorModal from "../ErrorModal";

const bound = {
  padding: "15px",
  border: "1px",
  backgroundColor: "#F2F2F2",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const boundDeployed = {
  padding: "15px",
  border: "1px",
  backgroundColor: "#C2C2DF",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const tableStyle = {
  minWidth: "678px",
  marginLeft: "-15px",
  marginRight: "-15px"
}

const liStyle = {
  listStyleType: "none",
  marginBottom: "20px",
  marginLeft: 0
}

const tableNameStyle = {
  fontSize: "20pt",
  fontFamily: "Roboto",
  fontWeight: "Bold"
}

const spacer = {
  width: "50px",
}
const buttons = {}

const separator = {
  height: "20px"

}

const bdelete = {
  padding: "5px 10px 5px 10px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "red",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  color: "red"
}
const td = {
  fontFamily: "Roboto",
  fontSize: "12pt",
}
const firsttd = {
  fontSize: "12pt",
  fontFamily: "Roboto",
  paddingLeft: "15px"
}

const bedit = {
  padding: "5px 20px 5px 20px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "#1B2CCA",
  backgroundColor: "#1B2CCA",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  marginRight: "10px",
  color: "white"
}
const close = {
  float: "right",
  color: "grey",
  border: "1px",
  padding: "2px",
  borderRadius: "2px",
  fontSize: "10pt",
  borderStyle: "solid"
}

const badd = {
  marginTop: "20px",
  padding: "3px 20px 3px 20px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "#1B2CCA",
  backgroundColor: "#1B2CCA",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  marginRight: "10px",
  color: "white",
  zIndex: 500,
}

const EditCreateIndexModal = (props) => {

  const [error, setError] = useState(false)
  const [errorMessage, setErrorMessage] = useState("")
  const [unique, setUnique] = useState(props.data.unique)
  const [tableName, setTableName] = useState(props.data.tableName)
  const [constraintName, setConstraintName] = useState(props.data.name)
  const [inputList, setInputList] = useState(props.data.createIndexColumns.map(tc => {
    return <div><EditIndexColumn model={props.model} data={tc} v={props.v} ct={props.data.id} onChange={()=>update()}/>
    </div>
  }));



  const update = () => {
    axios.put(
        `https://odd-login-app.herokuapp.com/api/liquibase-generator/create-index/${props.data.id}?v=${props.v}&model=${props.model}`,
        {
          id: props.data.id,
          tableName: tableName,
          unique: unique,
          name: constraintName
        }, {withCredentials: true}).then((response)=>{
      if(response.data.status === "error"){
        setErrorMessage(response.data.message)
        setError(true)
      }else {

      axios.get(`https://odd-login-app.herokuapp.com/api/liquibase-generator/create-index/${props.data.id}`, {withCredentials: true}).then(response=>{
        var str= response.data.unique? "ui_"+response.data.tableName : "ix_" +response.data.tableName
        response.data.createIndexColumns.forEach(ci=>{
          str+="_"+ci.name
        })
        setConstraintName(str)

      axios.put(
          `https://odd-login-app.herokuapp.com/api/liquibase-generator/create-index/${props.data.id}?v=${props.v}&model=${props.model}`,
          {
            id: props.data.id,
            tableName: tableName,
            unique: unique,
            name: str
          }, {withCredentials: true})
        })
    }})
  }

  const addColumn = () => {
    axios.post(
        `https://odd-login-app.herokuapp.com/api/liquibase-generator/index-column?v=${props.v}&ct=${props.data.id}`,
        {
          name: "",
          type: "",
        }, {withCredentials: true}).then((response) => {
      setInputList(
          inputList.concat(<div><EditIndexColumn model={props.model} ct={props.data.id} v={props.v}
                                                 data={{
                                                   id: response.data,
                                                   name: "",
                                                 }} onChange={()=>update()}/></div>))
      update()
    })
  }

  return (
      <div className={`modal_wrapper ${props.isOpened ? 'open' : 'close'}`}>
        <div className="modal_body">
          <div className="box" style={props.deployed ? boundDeployed : bound}>
            <div style={close} onClick={props.onClose}>exit</div>
            <div style={tableNameStyle}>
              {constraintName}
            </div>
            <div style={separator}/>
            <label>Table name: </label> <input value={tableName} onChange={(e)=>setTableName(e.target.value)}/>
            <div style={separator}/>
            <input checked={unique} type="checkbox" onChange={(e)=>setUnique(e.target.checked)}/> <label>unique</label>
            <div style={separator}/>
            <button style={bedit} onClick={()=>{update(); update()}}>SAVE</button>
            <div style={separator}/>
            {inputList}
            <div style={separator}/>
            <div>
              <button style={badd} onClick={addColumn}>ADD COLUMN</button>
            </div>
          </div>
        </div>
        <ErrorModal isOpened={error} message={errorMessage} onClose = {()=>{setError(false)}}/>

      </div>
  );

};

export default EditCreateIndexModal;