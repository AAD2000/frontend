import React, {useState} from "react";
import axios from "axios";

const bound = {
    padding: "15px",
    border: "1px",
    backgroundColor: "#F2F2F2",
    borderColor: "#C2C2C2",
    borderStyle: "solid",
    borderRadius: "4px",
    minWidth: "200px"
}

const boundDeployed = {
    padding: "15px",
    border: "1px",
    backgroundColor: "#C2C2DF",
    borderColor: "#C2C2C2",
    borderStyle: "solid",
    borderRadius: "4px",
    minWidth: "200px"
}

const tableStyle = {
    minWidth: "678px",
    marginLeft: "-15px",
    marginRight: "-15px"
}

const liStyle = {
    listStyleType: "none",
    marginBottom: "20px",
    marginLeft: 0
}

const tableName = {
    fontSize: "20pt",
    fontFamily: "Roboto",
    fontWeight: "Bold"
}

const spacer = {
    width: "50px",
}
const buttons = {}

const separator = {
    height: "20px"

}

const bdelete = {
    padding: "5px 10px 5px 10px",
    border: "1px",
    borderRadius: "4px",
    borderColor: "red",
    borderStyle: "solid",
    fontFamily: "Roboto",
    fontSize: "10pt",
    color: "red"
}
const td = {
    fontFamily: "Roboto",
    fontSize: "12pt",
}
const firsttd = {
    fontSize: "12pt",
    fontFamily: "Roboto",
    paddingLeft: "15px"
}

const bedit = {
    padding: "5px 20px 5px 20px",
    border: "1px",
    borderRadius: "4px",
    borderColor: "#1B2CCA",
    backgroundColor: "#1B2CCA",
    borderStyle: "solid",
    fontFamily: "Roboto",
    fontSize: "10pt",
    marginRight: "10px",
    color: "white"
}
const close = {
    float: "right",
    color: "grey",
    border: "1px",
    padding: "2px",
    borderRadius: "2px",
    fontSize: "10pt",
    borderStyle: "solid"
}
const lableStyle = {
    marginLeft: "10px"
}

const DeleteDockerComposeModal = (props) => {

    const [modelName, setModelName] = useState("")


    const postData = () => {
        if (modelName === props.modelName) {
            axios.get(`https://odd-login-app.herokuapp.com/api/deploying-system/deploy/${props.id}?model=${props.modelName}&rollback=true&tag=0.0&force=true`, {withCredentials: true}).then(res => {
                axios.delete(`https://odd-login-app.herokuapp.com/api/liquibase-generator/docker-compose/${props.id}`, {withCredentials: true}).then(res1 => {
                    props.onClose()
                })
            })
        } else {
            setModelName("")
        }
    }


    return (
        <div className={`modal_wrapper ${props.isOpened ? 'open' : 'close'}`}>
            <div className="modal_body">

                <div style={bound}>
                    <div style={close} onClick={props.onClose}>exit</div>
                    <div style={separator}/>
                    <div>
                        <label style={lableStyle}>Repeat model name '{props.modelName}'</label> <input value={modelName}
                                                                                                       onChange={(e) => {
                                                                                                           setModelName(e.target.value)
                                                                                                       }}/>
                    </div>
                    <div style={separator}/>
                    <button style={bdelete} onClick={postData}>DELETE</button>
                </div>
            </div>
        </div>
    );

}

export default DeleteDockerComposeModal;