import React from "react";

const bound = {
  padding: "15px",
  border: "1px",
  backgroundColor: "#F2F2F2",
  marginRight: "10px",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const boundDeployed = {
  padding: "15px",
  border: "1px",
  marginRight: "10px",
  backgroundColor: "#C2C2DF",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const tableStyle = {
  minWidth: "668px",
  marginLeft: "-15px",
  marginRight: "-15px"
}

const liStyle = {
  listStyleType: "none",
  marginBottom: "20px",
  marginLeft: 0
}

const tableName = {
  fontSize: "20pt",
  fontFamily: "Roboto",
  fontWeight: "Bold"
}

const spacer = {
  width: "50px",
}
const buttons = {}

const separator = {
  height: "20px"

}

const bdelete = {
  padding: "5px 10px 5px 10px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "red",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  color: "red",
  zIndex: 500,
}
const td = {
  fontFamily: "Roboto",
  fontSize: "12pt",
}
const firsttd = {
  fontSize: "12pt",
  fontFamily: "Roboto",
  paddingLeft: "15px"
}

const bedit = {
  padding: "5px 20px 5px 20px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "#1B2CCA",
  backgroundColor: "#1B2CCA",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  marginRight: "10px",
  color: "white",
  zIndex: 500,
}
const close = {
  float: "right",
  color: "grey",
  border: "1px",
  padding: "2px",
  borderRadius: "2px",
  fontSize: "10pt",
  borderStyle: "solid",
  zIndex: "500"
}

function AddColumnModal(props) {

  return (
      <div  className={`modal_wrapper ${props.isOpened ? 'open' : 'close'}`}>
        <div className="modal_body">
          <div  style={props.deployed ? boundDeployed : bound}>
            <div style={close} onClick={()=>props.onClose()}>exit</div>

            <div style={tableName}>
              Add to {props.data.tableName}
            </div>
            <div style={separator}/>
            <div>

              <table style={tableStyle}
                     cellspacing="0">
                <tr className="tr">
                  <td style={firsttd}>{props.data.name}</td>
                  <td style={spacer}></td>
                  <td style={td}>{props.data.type}</td>
                  <td style={spacer}></td>
                  <td style={td}>
                    <label className="container">pk
                      <input type="checkbox"
                             checked={props.data.primaryKey == null ? false
                                 : props.data.primaryKey}/>
                      <span className="checkmark"></span>
                    </label>
                  </td>
                  <td style={td}>
                    <label className="container">unique
                      <input type="checkbox"
                             checked={props.data.unique == null ? false
                                 : props.data.unique}/>
                      <span className="checkmark"></span>
                    </label>
                  </td>
                  <td style={td}>
                    <label className="container">nullable
                      <input type="checkbox"
                             checked={props.data.nullable == null ? false
                                 : props.data.nullable}/>
                      <span className="checkmark"></span>
                    </label>
                  </td>
                  <td style={td}>
                    <label className="container">increment
                      <input type="checkbox"
                             checked={props.data.autoIncrement == null ? false
                                 : props.data.autoIncrement}/>
                      <span className="checkmark"></span>
                    </label>
                  </td>
                </tr>
              </table>

            </div>

          </div>

        </div>
      </div>
  );
}

export default AddColumnModal;