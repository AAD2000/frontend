import axios from "axios";
import React, {useState} from "react";
import ErrorModal from "../ErrorModal";

const bound = {
    padding: "15px",
    border: "1px",
    backgroundColor: "#F2F2F2",
    borderColor: "#C2C2C2",
    borderStyle: "solid",
    borderRadius: "4px",
    minWidth: "200px"
}

const separator = {
    height: "20px"

}

const nameLabel = {
    width: "104px",
    paddingRight: "16px",
    textAlign: "center"
}
const checkboxLabel = {
    width: "75px",
    textAlign: "center"
}

const tableNameStyle = {
    fontSize: "20pt",
    fontFamily: "Roboto",
    fontWeight: "Bold"
}

const bedit = {
    padding: "5px 20px 5px 20px",
    border: "1px",
    borderRadius: "4px",
    marginTop: "10px",
    marginLeft: "0px",
    borderColor: "#1B2CCA",
    backgroundColor: "#1B2CCA",
    borderStyle: "solid",
    fontFamily: "Roboto",
    fontSize: "10pt",
    color: "white"
}
const td = {
    fontSize: "12pt",
    fontFamily: "Roboto",
    width: "100px",
    marginRight: "15px",
}
const close = {
    float: "right",
    color: "grey",
    border: "1px",
    padding: "2px",
    borderRadius: "2px",
    fontSize: "10pt",
    borderStyle: "solid"
}

const checkbox = {
    marginLeft: "34px",
    marginRight: "33.5px"
}

const EditAddColumnModal = (props) => {

    const [error, setError] = useState(false)
    const [errorMessage, setErrorMessage] = useState("")
    const [increment, setIncrement] = useState(props.data.autoIncrement)
    const [nullable, setNullable] = useState(props.data.nullable)
    const [pk, setPk] = useState(props.data.primaryKey)
    const [unique, setUnique] = useState(props.data.unique)
    const [name, setName] = useState(props.data.name)
    const [tableName, setTableName] = useState(props.data.tableName)
    const [type, setType] = useState(props.data.type)

    const addTable = () => {
        axios.put(`https://odd-login-app.herokuapp.com/api/liquibase-generator/add-column/${props.data.id}?v=${props.v}&model=${props.model}`,
            {
                id: props.data.id,
                autoIncrement: increment,
                nullable: nullable,
                primaryKey: pk,
                unique: unique,
                name: name,
                tableName: tableName,
                type: type,
            }, {withCredentials: true}).then((req) => {
            if (req.data.status === "error") {
                setErrorMessage(req.data.message)
                setError(true)
            } else {
                props.onClose()
            }
        })
    }

    return (
        <div className={`modal_wrapper ${props.isOpened ? 'open' : 'close'}`}>
            <div className="modal_body">
                <div style={bound}>
                    <div style={close} onClick={props.onClose}>exit</div>
                    <div><label>Table name: </label><input value={tableName}
                                                           onChange={(e) => setTableName(
                                                               e.target.value)}/></div>
                    <div style={separator}/>

                    <table>
                        <tr>
                            <td style={nameLabel}>Name</td>
                            <td style={nameLabel}>type</td>
                            <td style={checkboxLabel}>pk</td>
                            <td style={checkboxLabel}>unique</td>
                            <td style={checkboxLabel}>nullable</td>
                            <td>increment</td>
                        </tr>
                    </table>
                    <input style={td} value={name}
                           onChange={(e) => {
                               if (e.target.value.match("^[A-Za-z0-9_]*$")) {
                                   setName(e.target.value)
                               }

                           }
                           }/>
                    <input style={td} value={type} onChange={(e) => {
                        if (e.target.value.match("^[A-Za-z0-9_()]*$")) {
                            setType(e.target.value)
                        }
                    }
                    }/>
                    <input style={checkbox} checked={pk} onChange={(e) => {
                        setPk(e.target.checked)
                    }} type="checkbox"/>
                    <input style={checkbox} checked={unique} onChange={(e) => {
                        setUnique(e.target.checked)
                    }} type="checkbox"/>
                    <input style={checkbox} checked={nullable} onChange={(e) => {
                        setNullable(e.target.checked)
                    }} type="checkbox"/>
                    <input style={checkbox} checked={increment} onChange={(e) => {
                        setIncrement(e.target.checked)
                    }} type="checkbox"/>
                    <div>
                        <button style={bedit} onClick={() => {
                            addTable();
                        }}>SAVE
                        </button>
                    </div>
                </div>
            </div>
            <ErrorModal isOpened={error} message={errorMessage} onClose = {()=>{setError(false)}}/>
        </div>
    );
}

export default EditAddColumnModal