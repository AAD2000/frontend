import axios from "axios";
import React, {useState} from "react";

const bound = {
  padding: "15px",
  border: "1px",
  backgroundColor: "#F2F2F2",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const separator = {
  height: "20px"

}

const nameLabel = {
  width: "104px",
  paddingRight: "16px",
  textAlign: "center"
}
const checkboxLabel = {
  width: "75px",
  textAlign: "center"
}

const tableNameStyle = {
  fontSize: "20pt",
  fontFamily: "Roboto",
  fontWeight: "Bold"
}
const bdelete = {
  padding: "5px 10px 5px 10px",
  border: "1px",
  float: "right",
  borderRadius: "4px",
  borderColor: "red",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  color: "red"
}
const bedit = {
  padding: "5px 20px 5px 20px",
  border: "1px",
  borderRadius: "4px",
  marginTop: "10px",
  marginLeft: "0px",
  borderColor: "#1B2CCA",
  backgroundColor: "#1B2CCA",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  color: "white"
}
const td = {
  fontSize: "12pt",
  fontFamily: "Roboto",
  width: "100px",
  marginRight: "15px",
}
const close = {
  float: "right",
  color: "grey",
  border: "1px",
  padding: "2px",
  borderRadius: "2px",
  fontSize: "10pt",
  borderStyle: "solid"
}

const checkbox = {
  marginLeft: "34px",
  marginRight: "33.5px"
}
const textAreaStyle = {
  height: "700px",
  width: "642px",
  resize: "none"
}

const DeleteAddSqlModal = (props) => {

  const [sql, setSql] = useState(props.data.sql)

  const addSql = () => {
    axios.delete(`https://odd-login-app.herokuapp.com/api/liquibase-generator/add-sql/${props.data.id}`, {withCredentials: true}).then(() => props.onClose())
  }

  return (
      <div className={`modal_wrapper ${props.isOpened ? 'open' : 'close'}`}>
        <div className="modal_body">
          <div style={bound}>
            <div style={close} onClick={props.onClose}>exit</div>
            <div><label>Sql: </label></div>
            <div style={separator}/>
            <textarea style={textAreaStyle} value={sql}>

            </textarea>
            <div style={separator}/>
              <button style={bdelete} onClick={()=>{addSql()}}>DELETE</button>
            <div style={separator}/>
          </div>
        </div>
      </div>
  );
}

export default DeleteAddSqlModal