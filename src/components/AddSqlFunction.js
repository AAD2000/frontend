import React, {useState} from "react";

import CreateTableModal from "./modals/create-table/CreateTableModal";
import EditCreateTableModal from "./modals/create-table/EditCreateTableModal";
import DeleteTableModal from "./modals/create-table/DeleteTableModal";
import CreateConnectionModal
  from "./modals/create-connection/CreateConnectionModal";
import AddColumnModal from "./modals/add-column/AddColumnModal";
import DeleteAddColumnModal from "./modals/add-column/DeleteAddColumnModal";
import EditAddColumnModal from "./modals/add-column/EditAddColumnModal";
import EditAddSqlModal from "./modals/add-sql/EditAddSql";
import DeleteAddSqlModal from "./modals/add-sql/DeleteAddSql";
import AddSqlModal from "./modals/add-sql/AddSqlModal";
import DeleteAddSql from "./modals/add-sql/DeleteAddSql";

const bound = {
  padding: "15px",
  border: "1px",
  backgroundColor: "#F2F2F2",
  marginRight: "10px",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const boundDeployed = {
  padding: "15px",
  border: "1px",
  marginRight: "10px",
  backgroundColor: "#C2C2DF",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const tableStyle = {
  minWidth: "240px",
  marginLeft: "-15px",
  marginRight: "-15px"
}

const liStyle = {
  listStyleType: "none",
  marginBottom: "20px",
  marginLeft: 0
}

const tableName = {
  fontSize: "20pt",
  fontFamily: "Roboto",
  fontWeight: "Bold"
}

const spacer = {
  width: "50px",
}
const buttons = {}

const separator = {
  height: "20px"

}

const bdelete = {
  padding: "5px 10px 5px 10px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "red",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  color: "red",
  zIndex: 500,
}
const td = {
  fontFamily: "Roboto",
  fontSize: "12pt",
}
const firsttd = {
  fontSize: "12pt",
  fontFamily: "Roboto",
  paddingLeft: "15px"
}

const bedit = {
  padding: "5px 20px 5px 20px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "#1B2CCA",
  backgroundColor: "#1B2CCA",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  marginRight: "10px",
  color: "white",
  zIndex: 500,
}

function AddSqlFunction(props) {


  const [modal, setModal] = useState({
    modal1: {
      edit: false,
      mode: false,
      delete: false
    }
  })
  return (
      <li style={liStyle}>
        <div className="box" onClick={() => {
          setModal({modal1: {mode: true, edit: false, delete:false}})
        }} style={props.deployed ? boundDeployed : bound}>

          <div>
            <label>{props.data.sql.length > 23? props.data.sql.substring(0,20)+"..." : props.data.sql}</label>


          </div>
          <div style={separator}/>
          <div hidden={props.deployed} style={separator}/>
          <div hidden={props.deployed} style={buttons}>
            <button style={bedit} onClick={(event) => {
              setModal({...modal, modal1: {mode: false, edit: true, delete: false}})
              event.stopPropagation();
            }}>EDIT
            </button>
            <button className="delete"
                    style={bdelete} onClick={(event) => {
              setModal({...modal, modal1: {mode: false, edit: false, delete: true}})
              event.stopPropagation();
            }}>DELETE
            </button>
          </div>
        </div>

        <AddSqlModal isOpened={modal.modal1.mode} data={props.data}
                        onClose={() => {
                          setModal(
                              {
                                ...modal,
                                modal1: {
                                  mode: false,
                                  edit: false,
                                  delete: false
                                }
                              });

                        }}/>
        <DeleteAddSql isOpened={modal.modal1.delete} id={props.data.id} data={props.data} onClose={() => {
          setModal(
              {
                ...modal,
                modal1: {
                  mode: false,
                  edit: false,
                  delete: false
                }
              });
          props.update()
        }}/>
        <EditAddSqlModal isOpened={modal.modal1.edit} data={props.data} v={props.v} onClose={() => {
          setModal(
              {
                ...modal,
                modal1: {
                  mode: false,
                  edit: false,
                  delete: false
                }
              });
          props.update()
        }}/>
      </li>
  );
}

export default AddSqlFunction;