import React, {useState} from "react";
import EditCreateConnection
  from "./modals/create-connection/EditCreateConnection";
import CreateConnectionModal
  from "./modals/create-connection/CreateConnectionModal";
import axios from "axios";
import DeleteConnectionModal
  from "./modals/create-connection/DeleteConnectionModal";

const bound = {
  padding: "15px",
  border: "1px",
  backgroundColor: "#F2F2F2",
  marginRight: "10px",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const boundDeployed = {
  padding: "15px",
  border: "1px",
  marginRight: "10px",
  backgroundColor: "#C2C2DF",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const tableStyle = {
  minWidth: "240px",
  marginLeft: "-15px",
  marginRight: "-15px"
}

const liStyle = {
  listStyleType: "none",
  marginBottom: "20px",
  marginLeft: 0
}

const tableName = {
  fontSize: "20pt",
  fontFamily: "Roboto",
  fontWeight: "Bold"
}

const spacer = {
  width: "50px",
}
const buttons = {}

const separator = {
  height: "20px"

}

const bdelete = {
  padding: "5px 10px 5px 10px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "red",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  color: "red",
  zIndex: 500,
}
const td = {
  fontFamily: "Roboto",
  fontSize: "12pt",
  paddingLeft: "10px",
  paddingRight: "10px",
}
const firsttd = {
  fontSize: "12pt",
  fontFamily: "Roboto",
  paddingLeft: "15px"
}

const bedit = {
  padding: "5px 20px 5px 20px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "#1B2CCA",
  backgroundColor: "#1B2CCA",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  marginRight: "10px",
  color: "white",
  zIndex: 500,
}

function CreateConnectionFunction(props) {

  const delelteConnection = () => {
    axios.delete(`https://odd-login-app.herokuapp.com/api/liquibase-generator/connection/${props.data.id}`,{withCredentials: true})
  }

  const [modal, setModal] = useState({
    modal1: {
      edit: false,
      mode: false,
      delete: false
    }
  })
  return (
      <li style={liStyle}>
        <div className="box" onClick={() => {
          setModal({modal1: {mode: true, edit: false, delete: false}})
        }} style={props.deployed ? boundDeployed : bound}>
          <div style={tableName}>
            {props.data.constraintName}
          </div>
          <div style={separator}/>
          <table style={tableStyle}
                 cellSpacing="0">
            <tr className="tr">
              <td style={td}>
                Base table
              </td>
              <td style={td}>
                Base column
              </td>
              <td style={td}>
              </td>
              <td style={td}>
                Referenced table
              </td>
              <td style={td}>
                Referenced column
              </td>
            </tr>
            <tr className="tr">
              <td style={td}>
                {props.data.baseTableName}
              </td>
              <td style={td}>
                {props.data.baseColumnNames}
              </td>
              <td style={td}>
                →
              </td>
              <td style={td}>
                {props.data.referencedTableName}
              </td>
              <td style={td}>
                {props.data.referencedColumnNames}
              </td>
            </tr>
          </table>
          <div style={separator}/>
          <div hidden={props.deployed} style={separator}/>
          <div hidden={props.deployed} style={buttons}>
            <button style={bedit} onClick={(event) => {
              setModal(
                  {...modal, modal1: {mode: false, edit: true, delete: false}})
              event.stopPropagation();
            }}>EDIT
            </button>
            <button className="delete"
                    style={bdelete} onClick={(event) => {
              setModal(
                  {...modal, modal1: {mode: false, edit: false, delete: true}})
              event.stopPropagation();
            }}>DELETE
            </button>
          </div>
        </div>
        <EditCreateConnection model={props.model} v={props.v} isOpened={modal.modal1.edit}
                              data={props.data}
                              onClose={() => {
                                setModal(
                                    {
                                      ...modal,
                                      modal1: {
                                        mode: false,
                                        edit: false,
                                        delete: false
                                      }
                                    });
                                props.update()
                              }}/>
        <CreateConnectionModal isOpened={modal.modal1.mode} data={props.data}
                               onClose={() => {
                                 setModal(
                                     {
                                       ...modal,
                                       modal1: {
                                         mode: false,
                                         edit: false,
                                         delete: false
                                       }
                                     });

                               }}/>
        <DeleteConnectionModal model={props.model} id={props.data.id}
                               constraintName={props.data.constraintName}
                               isOpened={modal.modal1.delete} onClose={() => {
          setModal(
              {...modal, modal1: {mode: false, edit: false, delete: false}});
          props.update()
        }}/>
      </li>
  );
}

export default CreateConnectionFunction;