import React, {useLayoutEffect, useState} from "react";
import axios from "axios";
import loading from "../assets/loader.gif";
import loadingGif from "../assets/loader.gif";
import CreateDockerComposeModel from "./modals/CreateDockerComposeModel";
import DockerComposeWrapper from "./DockerComposeWrapper";
import DeleteDockerComposeModal from "./modals/DeleteDockerComposeModal";

const listStyle = {
  float: "none",
  margin: "auto",
  width: "700px",
}

const loadingStyle = {
  margin: "auto",
  position: "absolute",
  top: "10%",
  left: 0,
  right: 0,
};

const textStyle = {
  fontWeight: "bold",
}

const elementsStyle = {
  borderBottom: "0px",
  borderLeft: "0px",
  borderRight: "0px",
  borderTop: "1px",
  height: "25px",
  padding: "10px",
  borderColor: "lightgrey",
  borderStyle: "solid"
}

const bedit = {
  padding: "5px 20px 5px 20px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "#1B2CCA",
  backgroundColor: "#1B2CCA",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  marginRight: "10px",
  color: "white",
  marginTop: "10px",
  marginBottom: "10px",
  zIndex: 500,
}

const wrapperStyle = {
  marginTop: "55px"
}

const bdelete = {
  position: "relative",
  float: "right",
  padding: "5px 10px 5px 10px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "red",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  color: "red",
  zIndex: 500,
}

// const url = "https://odd-login-app.herokuapp.com/api/liquibase-generator/docker-compose?owner=aadanilov00%40gmail.com"
const url = "https://odd-login-app.herokuapp.com/api/liquibase-generator/docker-compose"

const AllDockerComposes = () => {
  const [id, setId] = useState(null)
  const [data, setData] = useState([]);
  const mapNameId = new Object()
  const [deleteId, setdeleteId] = useState(-1)
  const [deleteModalName, setDeleteModalName] = useState("")
  const [deleteModal, setDeleteModal] = useState(false)
  const [isLoadind, setLoading] = useState(true)
  const [model, setModel] = useState(false);

  const fetchUrl=()=>{
    axios.get(url, {withCredentials: true}).then(
        response => {console.log(response);setData(response.data); setLoading(false)})

  }

  useLayoutEffect(
      () =>{
        axios.get("https://odd-login-app.herokuapp.com/userinfo",{withCredentials: true}).catch((error) => {
          console.log(error.response)
          if (error?.response?.status === 403) {
            window.location.assign("https://odd-login-app.herokuapp.com/oauth2/authorization/keycloak")
            // window.location.assign("http://localhost:8080/auth/realms/odd/protocol/openid-connect/auth?response_type=code&client_id=login-app&state=123&redirect_uri=http://localhost:3001")
          }
        })
      }, []);

  const setRows = () => {
    return data.map(value => {
      mapNameId[value.modelName] = value.id;

      return (
          <div style={elementsStyle} onClick={(e) => setId(value.id)}>
            <label style={textStyle}>{value.modelName}</label>
            <button className="delete" style={bdelete} onClick={(e)=>{ setdeleteId(value.id); setDeleteModalName(value.modelName);setDeleteModal(true); e.stopPropagation()}}>DELETE</button>

          </div>);
    })
  }

  if (isLoadind) {
    fetchUrl();
  }

  return (
      <div style={wrapperStyle}>
        {
          isLoadind ?
              <img
                  style={loadingStyle}
                  src={loadingGif}
                  alt="loading..."
                  hidden={!loading}
              /> :
              <div>
                <div style={listStyle} hidden={id != null}>
                  <button style={bedit} onClick={() => setModel(true)}>
                    NEW
                  </button>
                  {setRows()}
                </div>
                <div hidden={id == null}>
                  {id == null ? <div/> : <DockerComposeWrapper id={id}
                                                               owner="aad"
                                                               close={(id) => {
                                                                 var killId = setTimeout(function() {
                                                                   for (var i = killId; i > 0; i--) clearInterval(i)
                                                                 }, 3000);
                                                                 setLoading(
                                                                     true);
                                                                 setId(null)
                                                               }}/>}

                </div>
                <CreateDockerComposeModel owner="aad" isOpened={model}
                                          setIdInModel={(id) => {
                                            setId(id)
                                          }} onClose={() => {
                  setModel(false);
                }}/>
              </div>
        }
        <DeleteDockerComposeModal isOpened={deleteModal} id={deleteId} modelName = {deleteModalName} onClose ={()=>{setLoading(
            true); fetchUrl(); setDeleteModal(false)}}/>
      </div>)
}

export default AllDockerComposes;