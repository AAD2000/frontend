import React, {useState} from "react";

import AddCreateConnectionModal from "./modals/create-connection/AddCreateConnectionModal";
import AddCreateIndexModal from "./modals/create-index/AddCreateIndexModal";

const bound = {
  padding: "15px",
  border: "2px",
  backgroundColor: "rgba(242,242,242,0.42)",
  marginRight: "10px",
  borderColor: "#C2C2C2",
  borderStyle: "dashed",
  borderRadius: "4px",
  minWidth: "200px"
}

const liStyle = {
  listStyleType: "none",
  marginBottom: "20px",
  marginLeft: 0,
  width: "300px",
}

const td = {
  fontFamily: "Roboto",
  fontSize: "43pt",
  color: "#C2C2C2",
  width: "300px",
  height: "150px",
  textAlign: "center"
}

function AddCreateIndex(props) {

  const [modal, setModal] = useState(false)
  return (
      <li style={liStyle}>
        <div className="box" onClick={() => {
          setModal(true)
        }} style={bound} hidden={props.deployed}>
          <table>
            <tr>
              <td style={td}>
                +
              </td>
            </tr>
          </table>
        </div>
        <AddCreateIndexModal model={props.model} v={props.v} isOpened={modal} onClose={() => {

          setModal(false)
          props.update()

        }}/>
      </li>
  );
}

export default AddCreateIndex;