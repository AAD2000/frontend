import React, {useState} from "react";
import axios from "axios";
import loading from "../assets/loader.gif"
import VersionComponent from "./VersionComponent";

const loadingStyle = {
  margin: "auto",
  position: "absolute",
  top: "10%",
  left: 0,
  right: 0,
};

const mainBody = {
  margin: "20px"
}

const DockerCompose = (props) => {

  const [data, setData] = useState(props.data)
  const [isLoading, setIsLoading] = useState(props.loading)



  const populateRowsWithData = () => {

    if (data == null) {
      return "";
    }
    const articleData = data.versions.map((article) => {
      return (<VersionComponent model={data.modelName} onRollback={(rollback, tag, force)=>props.onRollback(rollback, tag, force)} data={article} update={props.update
      }/>);
    });

    return articleData;
  };



  return (

      <div style={mainBody}>
        {populateRowsWithData()}
      </div>
  );

};

export default DockerCompose;
