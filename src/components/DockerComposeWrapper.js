import React, {useEffect, useState} from "react";
import axios from "axios";
import DockerCompose from "./DockerCompose";
import loadingGif from "../assets/loader.gif"
import CreateVersionModel from "./modals/CreateVersionModel";
import CreateDockerComposeModel from "./modals/CreateDockerComposeModel";
import LogComponent from "./LogComponent";
import ErrorMessageModal from "./modals/ErrorMessageModal";
import FileDownload from "js-file-download"

const loadingStyle = {
    margin: "auto",
    position: "absolute",
    top: "10%",
    left: 0,
    right: 0,
};

const wrapperStyle = {
    position: "fixed",
    top: "52px",
    float: "left",
    width: "54px",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "lightgrey"
}

const deployStyle = {
    position: "fixed",
    top: "52px",
    left: "54px",
    float: "none",
    verticalAlign: "center",
    width: "100%",
    height: "54px",
    display: "flex",
    alignItems: "center",
    backgroundColor: "lightgrey",
}

const mainBody = {
    marginTop: "116px",
    marginLeft: "54px"
}

const tdStyle = {
    width: "300px"
}

const backStyle = {
    fontSize: "30px",
    color: "black",
    padding: "10px",
    border: "none",
    width: "54px",
    height: "54px",
    backgroundColor: "transparent"
}
const logStyle = {
    fontSize: "20px",
    color: "black",
    padding: "10px",
    border: "none",
    width: "54px",
    height: "54px",
    backgroundColor: "transparent"
}

const deploymentItemStyle = {
    fontSize: "14px",
    fontFamily: "Courier New",
    textAlign: "center"
}


const DockerComposeWrapper = (props) => {
    const [loading, setLoading] = useState(true)
    const [data, setData] = useState(null)
    const [model, setModel] = useState(false)
    const [deployData, setDeployData] = useState({
        version: "",
        status: "",
        deployedClicked: false,
    })
    const [deployError, setDeployError] = useState(false)
    const [errorMessage, setErrorMessage] = useState("")
    const [errorTag, setErrorTag] = useState("")
    const [logDb, setLogDb] = useState({
        log: false,
        db: true,
    })
    const [modelName, setModelName] = useState("modelName")


    const fetchURL = () => {

        axios.get(
            `https://odd-login-app.herokuapp.com/api/liquibase-generator/docker-compose/${props.id}`, {withCredentials: true}).then(
            response => {
                setData(response.data);
            }).then(r => {
            setLoading(false);
        })


    }

    const createVersion = (major) => {


        axios.post(
            `https://odd-login-app.herokuapp.com/api/liquibase-generator/version?dc=${props.id}&major=${major}`, {sd: "sd"}, {withCredentials: true}).then(
            response => {
                setLoading(true)
                fetchURL()
            })
    }

    const downloadAdoc =()=>{
        axios.get(`https://odd-login-app.herokuapp.com/api/liquibase-generator/export/adoc?model=${data.modelName}`, {withCredentials: true})
            .then(res => {
                FileDownload(res.data, data.modelName+".adoc")

            })
    }

    const downloadZip = ()=>{
        window.location.assign(`https://odd-login-app.herokuapp.com/api/liquibase-generator/export/zip?model=${data.modelName}`)
    }

    const deploy = (rollback, tag, force) => {
        axios.get(`https://odd-login-app.herokuapp.com/api/deploying-system/deploy/${data.id}?model=${data.modelName}&rollback=${rollback}&tag=${tag}&force=${force}`, {withCredentials: true}).then(res => {
            setDeployData({version: "pending", status: "pending", deployedClicked: true})
            if (res.data.status === "error"){
                setDeployError(true)
                setErrorMessage(res.data.message)
                setErrorTag(tag)
            }
        })
    }

    const doinAnotherThread = (mn) => {
        setInterval(
            () => {

                axios.get(`https://odd-login-app.herokuapp.com/api/deploying-system/status?model=${mn}`, {withCredentials: true}).then(res => {
                    setDeployData(
                        {
                            version: res.data.version,
                            status: res.data.status,
                        }
                    )
                })
            }, 5000
        )
    }

    useEffect(() => {
        axios.get(
            `https://odd-login-app.herokuapp.com/api/liquibase-generator/docker-compose/${props.id}`, {withCredentials: true}).then(
            response => {
                setData(response.data);
                doinAnotherThread(response.data.modelName)
                setModelName(response.data.modelName)
            },).then(r => {
            setLoading(false);
        })

    }, [])
    return (

        <div>
            <div style={wrapperStyle}>

                <div>
                    <button style={backStyle} onClick={() => {
                        props.close()
                    }}>
                        ←
                    </button>
                </div>
                <div>
                    <button style={backStyle} onClick={() => {
                        setModel(true)
                    }}>
                        +
                    </button>
                </div>
                <div>
                    <button style={backStyle} onClick={() => {
                        deploy(false, null, false)
                    }}>
                        ⇧
                    </button>
                </div>

                <div>
                    <button style={backStyle} onClick={() => {
                        deploy(true, "0.0", false)
                    }}>
                        ↺
                    </button>
                </div>
                <div>
                    <button style={logStyle} onClick={() => {
                        const pre = logDb;
                        setLogDb({log: !pre.log, db: !pre.db})
                    }}>
                        {logDb.log ? "db" : "log"}
                    </button>
                </div>
                <div>
                    <button style={logStyle} onClick={() => {
                        downloadAdoc()
                    }}>
                        adoc
                    </button>
                </div>
                <div>
                    <button style={logStyle} onClick={() => {
                        downloadZip()
                    }}>
                        zip
                    </button>
                </div>
            </div>
            <div style={deployStyle}>
                <table>
                    <tr>
                        <td style={tdStyle}>
                            <button style={deploymentItemStyle} onClick={() => {
                                setLoading(true);
                                fetchURL();
                            }}>update
                            </button>
                        </td>
                        <td style={tdStyle}>
                            <label style={deploymentItemStyle}>Current version: {deployData.version}</label>
                        </td>
                        <td style={tdStyle}>
                            <label style={deploymentItemStyle}>Status: {deployData.status}</label>
                        </td>
                    </tr>
                </table>
            </div>
            {loading ? <img
                    style={loadingStyle}
                    src={loadingGif}
                    alt="loading..."
                    hidden={!loading}
                /> :
                <div style={mainBody}> {!logDb.log ? <DockerCompose onRollback={(rollback, tag, force) => {
                        deploy(rollback, tag, force)
                    }} data={data} update={() => {
                        setLoading(true);
                        fetchURL();
                    }} loading={loading} id={props.id}/> :
                    <LogComponent modelName={modelName}/>}
                </div>
            }
            <CreateVersionModel id={props.id} isOpened={model} onClose={() => {
                setModel(false);
            }} update={() => {
                setLoading(true);
                fetchURL();
            }}/>
            <ErrorMessageModal isOpened={deployError}  onClose ={()=>
                setDeployError(false)
            }
                               message = {errorMessage}
                               func={()=> deploy(true, errorTag,true)}
            />
            }
        </div>
    );

};

export default DockerComposeWrapper;
