import React from "react";
import background from "../assets/background.png"
import docker from "../assets/docker.png"
import liquibase from "../assets/liquibase.png"

const imgStyle = {
  width: "100%",
  height: "100%",
  marginTop: "50px",
  backgroundImage: `url(${background})`,
  position: "fixed",
  backgroundSize: "cover",
}
const textStyle = {
  paddingTop: "10%",
  height: "100%",
  fontFamily: "Roboto",
  fontStyle: "normal",
  fontWeight: "bold",
  color: "white",
  fontSize: "36px",
  lineHeight: "42px",
  float: "none",
  textAlign: "center",
  margin: "auto",
}

const image= {
  height: "200px",
  marginRight: "50px",
  marginLeft: "50px",
}

const separator = {
  height: "50px"
}


const startColour = {
  color: "white"
}


const MainPage = (props) => {
  return (<div style={imgStyle}>
    <div style={textStyle}>
      <div>The simplest way for database creation</div>
      <div style={separator}/>
      <div>Powered by</div>
      <div style={separator}/>
      <div>
        <img style={image} src={docker}/>
        <img style={image} src={liquibase}/>
      </div>
      <div style={separator}/>
      <a style={startColour} href="/databases">click here to start</a>

    </div>

  </div>)

};

export default MainPage;
