import {useEffect, useState} from "react";
import axios from "axios";

const wrapperStyle={
    paddingTop:"1px",
    paddingLeft: "30px",
    paddingRight: "30px",
    paddingBottom: "30px"
}

const textAreaStyle={
    width: "100%",
    height: "400px",
    resize: "none",
}

const LogComponent = (props) => {

    const [dblog, setDblog] = useState("")
    const [liqlog, setLiqlog] = useState("")
    const [dclog, setDclog] = useState("")

    const fetchData = () => {
        axios.get(`https://odd-login-app.herokuapp.com/api/deploying-system/status?model=${props.modelName}`, {withCredentials: true}).then(res => {
            if (res.data.dbLog) {
                axios.get(`https://odd-login-app.herokuapp.com/api/deploying-system/logs/database?model=${props.modelName}`, {withCredentials: true}).then(res1 => {
                    setDblog(res1.data.message);
                })
            }

            if (res.data.liquibaseLog) {
                axios.get(`https://odd-login-app.herokuapp.com/api/deploying-system/logs/liquibase?model=${props.modelName}`, {withCredentials: true}).then(res1 => {
                    setLiqlog(res1.data.message);
                })
            }

            if (res.data.dockerComposeLog) {
                axios.get(`https://odd-login-app.herokuapp.com/api/deploying-system/logs/docker-compose?model=${props.modelName}`, {withCredentials: true}).then(res1 => {
                    setDclog(res1.data.message);
                })
            }
        })
    }

    fetchData()
    return (
        <div style={wrapperStyle}>
            {dblog === "" ? <div/> : <div><h1>Database log</h1><div><textarea style={textAreaStyle}>{dblog}</textarea></div></div>}
            {liqlog === "" ? <div/> : <div><h1>Liquibase log</h1><div><textarea style={textAreaStyle}>{liqlog}</textarea></div></div>}
            {dclog === "" ? <div/> : <div><h1>Docker-compose log</h1><div><textarea style={textAreaStyle}>{dclog}</textarea></div></div>}
        </div>
    )
};

export default LogComponent;
