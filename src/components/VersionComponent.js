import React from "react";
import CreateTableFunction from "./CreateTableFunction";
import AddCreateTable from "./AddCreateTable";
import CreateConnectionFunction from "./CreateConnectionFunction";
import AddCreateConnection from "./AddCreateConnection";
import axios from "axios";
import AddAddColumn from "./AddAddColumn";
import AddColumnFunction from "./AddColumnFunction";
import AddAddSql from "./AddAddSql";
import AddSqlFunction from "./AddSqlFunction";
import AddCreateIndex from "./AddCreateIndex";
import CreateIndexFunction from "./CreateIndexFunction";
import AddDropTable from "./AddDropTable";
import DropTableFunction from "./DropTableFunction";
import DropIndexFunction from "./DropIndexFunction";
import AddDropIndex from "./AddDropIndex";
import AddDropConnection from "./AddDropConnection";
import DropConnectionFunction from "./DropConnectionFunction";
import AddDropColumn from "./AddDropColumn";
import DropColumnFunction from "./DropColumnFunction";

const versionName = {
  fontSize: "30pt",
  fontFamily: "Roboto",
  fontWeight: "Bold"
}

const createTableName = {
  fontSize: "20pt",
  fontFamily: "Roboto",
  fontWeight: "Bold"
}

const ulStyle = {
  paddingTop: "8px",
  display: "flex",
  flexWrap: "wrap",
  paddingLeft: 0

}

const versionLimmiter = {
  height: "6px",
  width: "300px",
  backgroundColor: "#1B2CCA"
}
const rollbackStyle = {
  color: "#1B2CCA",
  backgroundColor: "white",
  borderLeft: "0px",
  borderTop: "0px",
  borderRight: "0px",
  borderBottom: "1px solid #1B2CCA"
}

const bdelete = {
  padding: "5px 10px 5px 10px",
  border: "1px",
  marginBottom: "10px",
  borderRadius: "4px",
  borderColor: "red",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  color: "red",
  zIndex: 500,
}

class VersionComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      model: props.model,
      data: props.data,
      update: props.update,
      createTableHidden: props.data.deployed,
      createConnectionHidden: props.data.deployed,
      addColumnHidden: props.data.deployed,
      createIndexHidden: props.data.deployed,
      addSqlHidden: props.data.deployed,
      dropTableHidden: props.data.deployed,
      dropConnectionHidden: props.data.deployed,
      dropColumnHidden: props.data.deployed,
      dropIndexHidden: props.data.deployed,
      versionHidden: props.data.deployed,
    }
  }

  renderCreateTables = () => {
    const d = this.state.data.createTables.map(ct => {
      return (
          <CreateTableFunction model={this.state.model} update={this.state.update} v={this.state.data.id}
                               deployed={this.state.data.deployed} data={ct}/>)
    })
    return d;
  }

  renderCreateConnection = () => {
    const d =this.state.data.connections.map(c =>{
      return (
          <CreateConnectionFunction model={this.state.model} update={this.state.update} v={this.state.data.id}
                      deployed={this.state.data.deployed} data = {c}/>)
    })
    return d;
  }

  renderAddColumn = () => {
    const d = this.state.data.addColumns.map(ac=>{
      return <AddColumnFunction model={this.state.model} update={this.state.update} v={this.state.data.id}
                                deployed={this.state.data.deployed} data = {ac}/>
    })
    return d;
  }

  renderAddSql = () => {
    const d = this.state.data.addSqls.map(as=>{
      return <AddSqlFunction model={this.state.model} update={this.state.update} v={this.state.data.id}
                                deployed={this.state.data.deployed} data = {as}/>
    })
    return d;
  }

  renderCreateIndex = () => {
    const d = this.state.data.createIndices.map(ci=>{
      return <CreateIndexFunction model={this.state.model}
      update={this.state.update} v={this.state.data.id}
      deployed={this.state.data.deployed} data = {ci}/>
    })
    return d;
  }

  renderDropTable = () => {
    const d = this.state.data.dropTables.map(ci=>{
      return <DropTableFunction model={this.state.model}
      update={this.state.update} v={this.state.data.id}
      deployed={this.state.data.deployed} data = {ci}/>
    })
    return d;
  }

  renderDropIndex = ()=>{
    const d = this.state.data.dropIndices.map(ci=>{
      return <DropIndexFunction model={this.state.model}
          update={this.state.update} v={this.state.data.id}
          deployed={this.state.data.deployed} data = {ci}/>
    })
    return d;
  }

  renderDropConnection = ()=>{
    const d = this.state.data.dropConnections.map(ci=>{
      return <DropConnectionFunction model={this.state.model}
          update={this.state.update} v={this.state.data.id}
          deployed={this.state.data.deployed} data = {ci}/>
    })
    return d;
  }

  renderDropColumn = ()=>{
    const d = this.state.data.dropColumns.map(ci=>{
      return <DropColumnFunction model={this.state.model}
          update={this.state.update} v={this.state.data.id}
          deployed={this.state.data.deployed} data = {ci}/>
    })
    return d;
  }

  deleteVersion=()=>{
    axios.delete(`https://odd-login-app.herokuapp.com/api/liquibase-generator/version/${this.state.data.id}`, {withCredentials: true}).then((r)=>{
      this.state.update()
    })
  }

  checkVersionShow=()=>{
    if(!this.state.data.deployed){
      return false;
    }

    if (this.state.data.createTables.length === 0 &&
        this.state.data.connections.length === 0 &&
        this.state.data.addColumns.length === 0 &&
        this.state.data.addSqls.length === 0 &&
        this.state.data.createIndices.length === 0 &&
        this.state.data.dropTables.length === 0 &&
        this.state.data.dropIndices.length === 0 &&
        this.state.data.dropConnections.length === 0 &&
        this.state.data.dropColumns.length === 0){
      return true
    }
  }

  render() {
    return (
        <div>
        <div hidden={this.checkVersionShow()}>
          <label style={versionName}>
            Ver. {this.state.data.versionName}   </label>
          <button style={rollbackStyle} onClick={() => {
            this.props.onRollback(true, this.state.data.versionName, false)
          }} hidden={!this.state.data.deployed}>rollback to here
          </button>
          <button hidden={this.state.data.deployed} className="delete" style={bdelete} onClick={()=>this.deleteVersion()}>DELETE</button>

          <div style={versionLimmiter}/>

          <div>
            <div hidden={this.state.data.deployed && this.state.data.createTables.length === 0}>
            <h3 style={createTableName} onClick={()=>this.setState({ createTableHidden: !this.state.createTableHidden})}>
              {this.state.createTableHidden? ">": "v"} Create Table
            </h3>
            {this.state.createTableHidden ? <div/> :
                <ul style={ulStyle}>
                  {this.renderCreateTables()}
                  <AddCreateTable model={this.state.model}  update={this.state.update} v={this.state.data.id}
                                  deployed={this.state.data.deployed}/>
                </ul>
            }
            </div>

            <div hidden={this.state.data.deployed && this.state.data.connections.length === 0}>
            <h3 style={createTableName} onClick={()=>this.setState({ createConnectionHidden: !this.state.createConnectionHidden})}>
              {this.state.createConnectionHidden? ">": "v"} Create Connection
            </h3>
            {this.state.createConnectionHidden ? <div/> :
                <ul style={ulStyle}>
                  {this.renderCreateConnection()}
                  <AddCreateConnection model={this.state.model} update={this.state.update} v={this.state.data.id}
                                       deployed={this.state.data.deployed}/>
                </ul>
            }
            </div>

            <div hidden={this.state.data.deployed && this.state.data.addColumns.length === 0}>
            <h3 style={createTableName} onClick={()=>this.setState({ addColumnHidden: !this.state.addColumnHidden})}>
              {this.state.addColumnHidden? ">": "v"} Add column
            </h3>
            {this.state.addColumnHidden ? <div/> :
                <ul style={ulStyle}>
                  {this.renderAddColumn()}
                  <AddAddColumn model={this.state.model} update={this.state.update} v={this.state.data.id}
                                deployed={this.state.data.deployed}/>
                </ul>
            }
            </div>

            <div hidden={this.state.data.deployed && this.state.data.addSqls.length === 0}>
            <h3 style={createTableName} onClick={()=>this.setState({ addSqlHidden: !this.state.addSqlHidden})}>
              {this.state.addSqlHidden? ">": "v"} Add sql
            </h3>
            {this.state.addSqlHidden ? <div/> :
                <ul style={ulStyle}>
                  {this.renderAddSql()}
                  <AddAddSql model={this.state.model} update={this.state.update} v={this.state.data.id}
                             deployed={this.state.data.deployed}/>
                </ul>
            }
            </div>

            <div hidden={this.state.data.deployed && this.state.data.createIndices.length === 0}>
            <h3 style={createTableName} onClick={()=>this.setState({ createIndexHidden: !this.state.createIndexHidden})}>
              {this.state.createIndexHidden? ">": "v"} Create Index
            </h3>
            {this.state.createIndexHidden ? <div/> :
                <ul style={ulStyle}>
                  {this.renderCreateIndex()}
                  <AddCreateIndex model={this.state.model} update={this.state.update} v={this.state.data.id}
                                  deployed={this.state.data.deployed}/>
                </ul>
            }
            </div>

            <div hidden={this.state.data.deployed && this.state.data.dropTables.length === 0}>
            <h3 style={createTableName} onClick={()=>this.setState({ dropTableHidden: !this.state.dropTableHidden})}>
              {this.state.dropTableHidden? ">": "v"} Drop table
            </h3>
            {this.state.dropTableHidden ? <div/> :
                <ul style={ulStyle} >
                  {this.renderDropTable()}
                  <AddDropTable model={this.state.model} update={this.state.update}
                                v={this.state.data.id}
                                deployed={this.state.data.deployed}/>
                </ul>
            }
            </div>

            <div hidden={this.state.data.deployed && this.state.data.dropIndices.length === 0}>
            <h3 style={createTableName} onClick={()=>this.setState({ dropIndexHidden: !this.state.dropIndexHidden})}>
              {this.state.dropIndexHidden? ">": "v"} Drop index
            </h3>
            {this.state.dropIndexHidden? <div/> :
            <ul style={ulStyle}>
              {this.renderDropIndex()}
              <AddDropIndex model={this.state.model} update={this.state.update} v={this.state.data.id}
                                   deployed={this.state.data.deployed}/>
            </ul>}
            </div>

            <div hidden={this.state.data.deployed && this.state.data.dropConnections.length === 0}>
            <h3 style={createTableName} onClick={()=>this.setState({ dropConnectionHidden: !this.state.dropConnectionHidden})}>
              {this.state.dropConnectionHidden? ">": "v"} Drop connection
            </h3>
            {this.state.dropConnectionHidden? <div/>:
            <ul style={ulStyle}>
              {this.renderDropConnection()}
              <AddDropConnection model={this.state.model} update={this.state.update} v={this.state.data.id}
                                   deployed={this.state.data.deployed}/>
            </ul>}
            </div>

            <div hidden={this.state.data.deployed && this.state.data.dropColumns.length === 0}>
            <h3 style={createTableName} onClick={()=>this.setState({ dropColumnHidden: !this.state.dropColumnHidden})}>
              {this.state.dropColumnHidden? ">": "v"} Drop column
            </h3>
            {this.state.dropColumnHidden ? <div/> :
                <ul style={ulStyle}>
                  {this.renderDropColumn()}
                  <AddDropColumn model={this.state.model} update={this.state.update} v={this.state.data.id}
                                 deployed={this.state.data.deployed}/>
                </ul>
            }
            </div>


          </div>
        </div>
        </div>
    );
  }

}

export default VersionComponent;