import React, {useState} from "react";

import axios from "axios";
import DropIndexModal from "./modals/drop-index/DropIndexModal";

const bound = {
  padding: "15px",
  border: "1px",
  backgroundColor: "#F2F2F2",
  marginRight: "10px",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const boundDeployed = {
  padding: "15px",
  border: "1px",
  marginRight: "10px",
  backgroundColor: "#C2C2DF",
  borderColor: "#C2C2C2",
  borderStyle: "solid",
  borderRadius: "4px",
  minWidth: "200px"
}

const tableStyle = {
  minWidth: "240px",
  marginLeft: "-15px",
  marginRight: "-15px"
}

const liStyle = {
  listStyleType: "none",
  marginBottom: "20px",
  marginLeft: 0
}

const tableName = {
  fontSize: "20pt",
  fontFamily: "Roboto",
  fontWeight: "Bold"
}

const spacer = {
  width: "50px",
}
const buttons = {}

const separator = {
  height: "20px"

}

const bdelete = {
  padding: "5px 10px 5px 10px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "red",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  color: "red",
  zIndex: 500,
}
const td = {
  fontFamily: "Roboto",
  fontSize: "12pt",
}
const firsttd = {
  fontSize: "12pt",
  fontFamily: "Roboto",
  paddingLeft: "15px"
}

const bedit = {
  padding: "5px 20px 5px 20px",
  border: "1px",
  borderRadius: "4px",
  borderColor: "#1B2CCA",
  backgroundColor: "#1B2CCA",
  borderStyle: "solid",
  fontFamily: "Roboto",
  fontSize: "10pt",
  marginRight: "10px",
  color: "white",
  zIndex: 500,
}

function DropIndexFunction(props) {


  const [modal, setModal] = useState({
    modal1: {
      edit: false,
      mode: false,
      delete: false
    }
  })

  const deleteyId=()=>{
    axios.delete(`https://odd-login-app.herokuapp.com/api/liquibase-generator/drop-index/${props.data.id}`, {withCredentials: true}).then(
        props.update())
  }

  return (
      <li style={liStyle}>
        <div className="box" onClick={() => {
          setModal({modal1: {mode: true}})
        }} style={props.deployed ? boundDeployed : bound}>

          <h2>
            Drop Index: {props.data.name}
          </h2>
          <label>Table name: {props.data.tableName}</label>
          <div style={separator}/>
          <div hidden={props.deployed} style={separator}/>
          <div hidden={props.deployed} style={buttons}>
            <button className="delete"
                    style={bdelete} onClick={(event) => {
              deleteyId()

              event.stopPropagation();
            }}>DELETE
            </button>
          </div>

        </div>



        <DropIndexModal isOpened={modal.modal1.mode} data={props.data}
                        onClose={() => {
                          setModal(
                              {
                                ...modal,
                                modal1: {
                                  mode: false,
                                  edit: false,
                                  delete: false
                                }
                              });

                        }}/>
      </li>
  );
};

export default DropIndexFunction;