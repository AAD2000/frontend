import React, {useLayoutEffect} from "react";
import "./Navbar.css";
import {Nav, Navbar} from "react-bootstrap";
import {withRouter} from "react-router-dom";
import logo from "../assets/logo.png"
import axios from "axios";
import Cookies from "react-cookie"

const navBarStyle = {
  paddingTop: "15px",
  paddingBottom: "15px",
  paddingRight: "10px",
  paddingLeft: "15px",
  backgroundColor: "#1B2CCA",
};

const navLinkStyle = {
  margin: "10px",
  fontWeight: 600,
  fontSize: "14pt",
  color: "white",
  textDecoration: "none",
};

const logInStyle = {
  position: "absolute",
  right: "20px",
  height: "31px",
  marginTop: "0px",
  fontSize: "14pt",
  color: "white",
};

const logoStyle = {
  marginBottom: "-3px",
  width: "20px",
  height: "20px",
  marginRight: "5px"
}

const Navigation = (props) => {


  return (
      <Navbar style={navBarStyle} bg="primary" variant="dark">
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="/">
              <img src={logo} style={logoStyle}/>
            </Nav.Link>
            <Nav.Link style={navLinkStyle} href="/databases">
              Databases
            </Nav.Link>
            <button onClick={()=>{
                window.location.assign("https://odd-login-app.herokuapp.com/logout", {},{withCredentials: true})
              }
            }>
              logout
            </button>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
  );

};

export default withRouter(Navigation);
