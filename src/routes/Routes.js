import React, { Component } from "react";
import { Router, Switch, Route } from "react-router-dom";

import history from "./history"
import AllDockerComposes from "../components/AllDockerComposes";
import MainPage from "../components/MainPage";

export default class Routes extends Component {
    render() {
        return (
            <Router history={history}>
                <Switch>
                    <Route path="/" exact component={MainPage} />
                    <Route path="/databases" exact component={AllDockerComposes} />
                </Switch>
            </Router>
        )
    }
}