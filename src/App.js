import './App.css';
import AllDockerComposes from "./components/AllDockerComposes";
import Navigation from "./navigation/Navigation";
import { BrowserRouter as Router } from "react-router-dom";
import Routes from "./routes/Routes"

const style = {
  position: "fixed",
  width: "100%",
  top: "0"
}
function App() {
  return (
      <Router>
        <div>
          <div style={style}>
          <Navigation/>
          </div>
          <Routes/>
        </div>
      </Router>
  );
}

export default App;
